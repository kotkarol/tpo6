package zad1.BankServer;

import zad1.Common.BankClasses.Bank;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.rmi.RemoteException;
import java.util.Hashtable;

public class BankServer {

    public static void main(String[] args) {
        try {
            Bank bank = new Bank(args[0]);
            Hashtable env = new Hashtable(11);
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
            Context initCtx = new InitialContext(env);
            initCtx.rebind("BankService", bank);
        } catch (RemoteException | NamingException e) {
            e.printStackTrace();
        }
    }
}

package zad1.StoreServer;

import zad1.Common.StoreClasses.Store;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.rmi.RemoteException;
import java.util.Hashtable;

public class StoreServer {

    public static void main(String[] args) {
        try {
            Store store = new Store(args[0], args[1]);
            Hashtable env = new Hashtable(11);
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
            Context initCtx = new InitialContext(env);
            initCtx.rebind("StoreService", store);

        } catch (RemoteException | NamingException e) {
            e.printStackTrace();
        }
    }

}

package zad1.Common.BankClasses;

import zad1.Common.BankAccount;
import zad1.Common.Bill;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.naming.Referenceable;
import javax.naming.StringRefAddr;
import javax.rmi.PortableRemoteObject;
import java.rmi.RemoteException;
import java.util.ArrayList;

public class Bank extends PortableRemoteObject implements BankInterface, Referenceable {

    private ArrayList<BankAccount> bankInfos = new ArrayList<>();
    private String bankName;

    /**
     * Initializes the object by calling <code>exportObject(this)</code>.
     *
     * @throws RemoteException if export fails.
     */
    public Bank(String bankName) throws RemoteException {
        super();
        this.bankName = bankName;
    }

    /**
     * Retrieves the Reference of this object.
     *
     * @return The non-null Reference of this object.
     * @throws NamingException If a naming exception was encountered
     *                         while retrieving the reference.
     */
    @Override
    public Reference getReference() throws NamingException {
        return new Reference(Bank.class.getName(), new StringRefAddr("bank",
                this.getBankName()), BankFactory.class.getName(), null);

    }

    @Override
    public void ProcessBill(Bill bill) {
        System.out.println("test");
        BankAccount account = bankInfos.stream().filter(x -> x.getClientIdentifier().equals(bill.getClientIdentifier())).findFirst().orElse(null);
        if (account == null) {
            BankAccount newAccount = new BankAccount(bill.getClientIdentifier());
            bankInfos.add(newAccount);
            newAccount.AddNewBill(bill);
        } else {
            account.AddNewBill(bill);
        }
    }

    @Override
    public ArrayList<Bill> getBillsForClient(String clientIdentifier) {
        BankAccount account = bankInfos.stream().filter(x -> x.getClientIdentifier().equals(clientIdentifier)).findFirst().orElse(null);
        if (account != null) {
            return account.getBills();
        }

        return null;
    }

    public String getBankName() {
        return bankName;
    }
}

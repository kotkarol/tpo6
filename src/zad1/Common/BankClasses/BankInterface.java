package zad1.Common.BankClasses;

import zad1.Common.Bill;

import java.rmi.RemoteException;
import java.util.ArrayList;

public interface BankInterface extends java.rmi.Remote {

    void ProcessBill(Bill bill) throws RemoteException;

    ArrayList<Bill> getBillsForClient(String clientIdentifier) throws RemoteException;

}

package zad1.Common;

import zad1.Common.GoodClasses.Good;

import java.math.BigDecimal;
import java.util.Hashtable;

public class Bill {

    private String biilId;
    private final String clientIdentifier;
    private final String bankAccountNumber;
    private Hashtable<Good, Integer> goodsAndAmounts;

    public Bill(String clientIdentifier, String bankAccountNumber, Hashtable<Good, Integer> goodsAndAmounts) {
        this.goodsAndAmounts = goodsAndAmounts;
        this.biilId = biilId;
        this.clientIdentifier = clientIdentifier;
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getClientIdentifier() {
        return clientIdentifier;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public String getBiilId() {
        return biilId;
    }

    public void setBiilId(String biilId) {
        this.biilId = biilId;
    }

    public Hashtable<Good, Integer> getGoodsAndAmounts() {
        return goodsAndAmounts;
    }

    public BigDecimal getOrderSum()
    {
        BigDecimal sum = new BigDecimal(0);
        for (Good good : getGoodsAndAmounts().keySet())
        {
            sum = sum.add(good.getUnitValue().multiply(new BigDecimal(getGoodsAndAmounts().get(good))));
        }

        return sum;
    }
}

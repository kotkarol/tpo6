package zad1.Common;

import zad1.Common.GoodClasses.Good;

import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Set;

public class Order {

    private String clientIdentifier;
    private Hashtable<Good, Integer> goodsAndAmounts;
    private String clientBankAccountNumber;

    public Order(String clientIdentifier, Hashtable<Good, Integer> goodsAndAmounts, String clientBankAccountNumber)
    {
        this.clientIdentifier = clientIdentifier;
        this.goodsAndAmounts = goodsAndAmounts;
        this.clientBankAccountNumber = clientBankAccountNumber;
    }

    public String getClientIdentifier() {
        return clientIdentifier;
    }

    public String getClientBankAccountNumber() {
        return clientBankAccountNumber;
    }
    public BigDecimal GetOrderValue(){
        BigDecimal result = new BigDecimal(0);

        if(getGoodsAndAmounts() == null)
        {
            return result;
        }

        if(getGoodsAndAmounts().size() == 0)
        {
            return result;
        }
        Set<Good> keys = getGoodsAndAmounts().keySet();
        for (Good good : keys) {
            result = result.add(good.getUnitValue().multiply(new BigDecimal(getGoodsAndAmounts().get(good))));
        }

        return result;
    }

    public Hashtable<Good, Integer> getGoodsAndAmounts() {
        return goodsAndAmounts;
    }
}

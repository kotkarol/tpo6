package zad1.Common.GoodClasses;

import java.rmi.RemoteException;

public interface GoodInterface extends java.rmi.Remote {
    int getAmount() throws RemoteException;
    void increaseAmount(int amount) throws RemoteException;
}

package zad1.Common.GoodClasses;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.naming.Referenceable;
import javax.naming.StringRefAddr;
import javax.rmi.PortableRemoteObject;
import java.math.BigDecimal;
import java.rmi.RemoteException;

public class Good extends PortableRemoteObject implements GoodInterface, Referenceable {

    private BigDecimal unitValue;
    private String name;
    private int amount;

    public Good(String name, BigDecimal unitValue, int amount) throws RemoteException {
        super();
        this.name = name;
        this.unitValue = unitValue;
        this.amount = amount;
    }

    public BigDecimal getUnitValue() {
        return unitValue;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }
    public void increaseAmount(int amount)
    {
        this.amount += amount;
    }

    /**
     * Retrieves the Reference of this object.
     *
     * @return The non-null Reference of this object.
     * @throws NamingException If a naming exception was encountered
     *                         while retrieving the reference.
     */
    @Override
    public Reference getReference() throws NamingException {
        return new Reference(Good.class.getName(), new StringRefAddr("good",
                this.getName() + "," + this.getUnitValue() + "," + this.getAmount()), GoodFactory.class.getName(), null);
    }
}

package zad1.Common.StoreClasses;

import zad1.Common.Bill;
import zad1.Common.GoodClasses.Good;
import zad1.Common.Order;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.naming.Referenceable;
import javax.naming.StringRefAddr;
import javax.rmi.PortableRemoteObject;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class Store extends PortableRemoteObject implements StoreInterface, Referenceable {
    private String storeName;
    private String bankAccountNumber;
    private List<Good> shopGoods = new ArrayList<>();

    /**
     * Initializes the object by calling <code>exportObject(this)</code>.
     *
     * @throws RemoteException if export fails.
     */
    public Store(String storeName, String bankAccountNumber) throws RemoteException {
        super();
        this.storeName = storeName;
        this.bankAccountNumber = bankAccountNumber;

        this.addGood(new Good("GoodOne", new BigDecimal(10), 100));
        this.addGood(new Good("GoodTwo", new BigDecimal(12), 20));
        this.addGood(new Good("GoodThree", new BigDecimal(14), 55));
        this.addGood(new Good("GoodFour", new BigDecimal(8), 120));

    }

    /**
     * Retrieves the Reference of this object.
     *
     * @return The non-null Reference of this object.
     * @throws NamingException If a naming exception was encountered
     *                         while retrieving the reference.
     */
    @Override
    public Reference getReference() throws NamingException {
        return new Reference(Store.class.getName(), new StringRefAddr("store",
                this.getStoreName() + "," + this.getBankAccountNumber()), StoreFactory.class.getName(), null);
    }

    @Override
    public String getStoreName() {
        return storeName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    @Override
    public Bill GetBill(Order order) {
        return new Bill(order.getClientIdentifier(), this.bankAccountNumber, order.getGoodsAndAmounts());
    }

    @Override
    public void addGood(Good good) throws RemoteException {
        Good foundGood = this.getGoods().stream().filter(x -> x.getName().equals(good.getName())).findFirst().orElse(null);
        if(foundGood == null)
        {
            shopGoods.add(good);
        }
        else
        {
            foundGood.increaseAmount(good.getAmount());
        }
    }

    @Override
    public void decreaseGoodAmount(Good goodOne, Integer amount) throws RemoteException {
        Good foundGood = this.getGoods().stream().filter(x -> x.getName().equals(goodOne.getName())).findFirst().orElse(null);
        if(foundGood == null)
        {
            return;
        }
        else
        {
            foundGood.increaseAmount((-1) * amount);
        }
    }

    public List<Good> getGoods() throws RemoteException {
        return this.shopGoods;
    }
}

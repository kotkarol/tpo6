package zad1.Common.StoreClasses;

import zad1.Common.Bill;
import zad1.Common.GoodClasses.Good;
import zad1.Common.Order;

import java.rmi.RemoteException;
import java.util.List;

public interface StoreInterface extends java.rmi.Remote {

    Bill GetBill(Order order) throws RemoteException;

    void addGood(Good goodOne) throws RemoteException;

    void decreaseGoodAmount(Good goodOne, Integer amount) throws RemoteException;

    String getStoreName() throws RemoteException;

    List<Good> getGoods() throws RemoteException;
}

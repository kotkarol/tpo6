package zad1.Common;

import java.math.BigDecimal;
import java.util.ArrayList;

public class BankAccount {

    private String clientIdentifier;
    private ArrayList<Bill> bills = new ArrayList<>();

    public BankAccount(String clientIdentifier) {
        this.clientIdentifier = clientIdentifier;
    }

    public String getClientIdentifier() {
        return clientIdentifier;
    }

    public void AddNewBill(Bill bill)
    {
        if (!getClientIdentifier().equals(bill.getClientIdentifier()))
        {
            return;
        }

        bill.setBiilId(clientIdentifier + "/" + bills.size());
        bills.add(bill);
    }
    public ArrayList<Bill> getBills() {
        return this.bills;
    }
    public BigDecimal getBillsSum()
    {
        BigDecimal sum = new BigDecimal(0);

        for(Bill bill : getBills())
        {
            sum = sum.add(bill.getOrderSum());
        }
        return sum;
    }
}

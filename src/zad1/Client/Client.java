package zad1.Client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import zad1.Common.BankClasses.BankInterface;
import zad1.Common.StoreClasses.StoreInterface;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import java.rmi.RemoteException;
import java.util.Hashtable;

public class Client extends Application {

    public static BankInterface BankService;
    public static StoreInterface StoreService;
    public static String ClientIdentifier;

    public static void main(String[] args) throws NamingException, RemoteException {
        IntializeRemote();

        ClientIdentifier = "" + (int)Math.random();

        launch(args);
    }
    /**
     * The main entry point for all JavaFX applications.
     * The start method is called after the init method has returned,
     * and after the system is ready for the application to begin running.
     *
     * <p>
     * NOTE: This method is called on the JavaFX Application Thread.
     * </p>
     *
     * @param primaryStage the primary stage for this application, onto which
     *                     the application scene can be set. The primary stage will be embedded in
     *                     the browser if the application was launched as an applet.
     *                     Applications may create other stages, if needed, but they will not be
     *                     primary stages and will not be embedded in the browser.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Views/ClientView.fxml"));
        primaryStage.setTitle("TPO 6 - Client");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

    private static void IntializeRemote() throws NamingException, RemoteException {
        Hashtable env = new Hashtable(11);
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
        Context ic  = new InitialContext(env);

        Object bankService = ic.lookup("BankService");
        Object storeService = ic.lookup("StoreService");
        StoreService = (StoreInterface) PortableRemoteObject.narrow(storeService, StoreInterface.class);
        BankService = (BankInterface) PortableRemoteObject.narrow(bankService, BankInterface.class);
    }
}

package zad1.Client.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import zad1.Client.Client;
import zad1.Common.Bill;
import zad1.Common.GoodClasses.Good;
import zad1.Common.Order;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;

public class ClientController {

    @FXML
    public TableView<Good> GoodsTable;
    public TableColumn<Good, String> GoodTableColumnOne;
    public TableColumn<Good, BigDecimal> GoodTableColumnTwo;
    public TableColumn<Good, Integer> GoodTableColumnThree;
    public TextField ClientId;
    public TextField ClientBankAccout;
    public TableView<Bill> BillsTable;
    public TableColumn<Bill, String> BillTableColumnOne;
    public TableColumn<Bill, BigDecimal> BillTableColumnTwo;

    @FXML
    public void initialize() throws RemoteException {
        GoodTableColumnOne.setCellValueFactory(new PropertyValueFactory<>("Name"));
        GoodTableColumnTwo.setCellValueFactory(new PropertyValueFactory<>("UnitValue"));
        GoodTableColumnThree.setCellValueFactory(new PropertyValueFactory<>("Amount"));
        BillTableColumnOne.setCellValueFactory(new PropertyValueFactory<>("BiilId"));
        BillTableColumnTwo.setCellValueFactory(new PropertyValueFactory<>("OrderSum"));

        GoodsTable.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );

        SetGoodTableItems();
        new Thread( () ->
            {
                try {
                    Thread.sleep(2000);
                    SetGoodTableItems();
                } catch (InterruptedException | RemoteException e) {
                    e.printStackTrace();
                }
            }
        ).start();
    }

    private void SetGoodTableItems() throws RemoteException {
        List<Good> refGood = Client.StoreService.getGoods();
        GoodsTable.setItems(FXCollections.observableArrayList(refGood));
    }

    public void BuyButtonClicked() throws RemoteException {
        String clientId = getClientId(this.ClientId);
        if (clientId == null) return;

        String accountNumber = getClientId(this.ClientBankAccout);
        if (accountNumber == null) return;

        ObservableList<Good> selectedGoods = GoodsTable.getSelectionModel().getSelectedItems();
        Hashtable<Good, Integer> goodsAndAmounts = getGoodsAmountsFromSelection(selectedGoods);
        Order order = new Order(clientId, goodsAndAmounts, accountNumber);

        Bill bill = Client.StoreService.GetBill(order);
        Client.BankService.ProcessBill(bill);
    }

    private Hashtable<Good, Integer> getGoodsAmountsFromSelection(ObservableList<Good> selectedGoods) throws RemoteException {
        Hashtable<Good, Integer> goodsAndAmounts = new Hashtable<>();
        for (Good good : selectedGoods)
        {
            Integer amount = showInputTextDialog(good, false);
            Client.StoreService.decreaseGoodAmount(good, amount);
            goodsAndAmounts.put(good, amount);
        }
        return goodsAndAmounts;
    }

    private String getClientId(TextField clientId2) {
        String clientId = clientId2.getText();
        if (clientId == null || clientId.isEmpty()) {
            clientId2.setStyle("-fx-background-color: #fcccdb;");
            return null;
        }
        return clientId;
    }

    private int showInputTextDialog(Good good, boolean repeted) {

        TextInputDialog dialog = new TextInputDialog("1");
        dialog.setTitle("Amount seleciton");
        if(repeted)
        {
            dialog.setHeaderText("Provide correct requested amount of " + good.getName() + ":");
        }
        else {
            dialog.setHeaderText("Provide requested amount of " + good.getName() + ":");
        }
        dialog.setContentText("Requested amount:");

        Optional<String> result = dialog.showAndWait();
        if(result.isPresent())
        {
            return Integer.parseInt(result.get());
        }
        else {
            return showInputTextDialog(good, true);
        }
    }

    public void OnSummaryButtonClick() throws RemoteException {
        String clientId = getClientId(this.ClientId);
        if (clientId == null) return;

        ArrayList<Bill> bills = Client.BankService.getBillsForClient(clientId);
        BillsTable.setItems(FXCollections.observableArrayList(bills));
    }
}
